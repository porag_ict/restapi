

import Foundation

class RestManager {
    
    // MARK: - Properties
    
    var requestHttpHeaders = RestEntity()
    
    var urlQueryParameters = RestEntity()
    
    var httpBodyParameters = RestEntity()
    
    var httpBody: Data?
    
    
    // MARK: - Public Methods
    
    func makeRequest(toURL url: URL,
                     withHttpMethod httpMethod: HttpMethod,
                     completion: @escaping (_ result: Results) -> Void) {
        
        DispatchQueue.global(qos: .userInitiated).async { [weak self] in
            let targetURL = self?.addURLQueryParameters(toURL: url)
            let httpBody = self?.getHttpBody()
            
            guard let request = self?.prepareRequest(withURL: targetURL, httpBody: httpBody, httpMethod: httpMethod) else
            {
                completion(Results(withError: CustomError.failedToCreateRequest))
                return
            }
            
            let sessionConfiguration = URLSessionConfiguration.default
            let session = URLSession(configuration: sessionConfiguration)
            let task = session.dataTask(with: request) { (data, response, error) in
                completion(Results(withData: data,
                                   response: Response(fromURLResponse: response),
                                   error: error))
            }
            task.resume()
        }
    }
    
    
    
    func getData(fromURL url: URL, completion: @escaping (_ data: Data?) -> Void) {
        DispatchQueue.global(qos: .userInitiated).async {
            let sessionConfiguration = URLSessionConfiguration.default
            let session = URLSession(configuration: sessionConfiguration)
            let task = session.dataTask(with: url, completionHandler: { (data, response, error) in
                guard let data = data else { completion(nil); return }
                completion(data)
            })
            task.resume()
        }
    }
    
    
    
    // MARK: - Private Methods
    
    private func addURLQueryParameters(toURL url: URL) -> URL {
        if urlQueryParameters.totalItems() > 0 {
            guard var urlComponents = URLComponents(url: url, resolvingAgainstBaseURL: false) else { return url }
            var queryItems = [URLQueryItem]()
            for (key, value) in urlQueryParameters.allValues() {
                let item = URLQueryItem(name: key, value: value.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed))
                
                queryItems.append(item)
            }
            
            urlComponents.queryItems = queryItems
            
            guard let updatedURL = urlComponents.url else { return url }
            return updatedURL
        }
        
        return url
    }
    
    
    
    private func getHttpBody() -> Data? {
        guard let contentType = requestHttpHeaders.value(forKey: "Content-Type") else { return nil }
        
        if contentType.contains("application/json") {
            return try? JSONSerialization.data(withJSONObject: httpBodyParameters.allValues(), options: [.prettyPrinted, .sortedKeys])
        } else if contentType.contains("application/x-www-form-urlencoded") {
            let bodyString = httpBodyParameters.allValues().map { "\($0)=\(String(describing: $1.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)))" }.joined(separator: "&")
            return bodyString.data(using: .utf8)
        } else {
            return httpBody
        }
    }
    
    
    
    private func prepareRequest(withURL url: URL?, httpBody: Data?, httpMethod: HttpMethod) -> URLRequest? {
        guard let url = url else { return nil }
        var request = URLRequest(url: url)
        request.httpMethod = httpMethod.rawValue
        
        for (header, value) in requestHttpHeaders.allValues() {
            request.setValue(value, forHTTPHeaderField: header)
        }
        
        request.httpBody = httpBody
        return request
    }
}


// MARK: - RestManager Custom Types

extension RestManager {
    enum HttpMethod: String {
        case get
        case post
        case put
        case patch
        case delete
    }
    
    
    
    struct RestEntity {
        private var values: [String: String] = [:]
        
        mutating func add(value: String, forKey key: String) {
            values[key] = value
        }
        
        func value(forKey key: String) -> String? {
            return values[key]
        }
        
        func allValues() -> [String: String] {
            return values
        }
        
        func totalItems() -> Int {
            return values.count
        }
    }
    
    
    
    struct Response {
        var response: URLResponse?
        var httpStatusCode: Int = 0
        var headers = RestEntity()
        
        init(fromURLResponse response: URLResponse?) {
            guard let response = response else { return }
            self.response = response
            httpStatusCode = (response as? HTTPURLResponse)?.statusCode ?? 0
            
            if let headerFields = (response as? HTTPURLResponse)?.allHeaderFields {
                for (key, value) in headerFields {
                    headers.add(value: "\(value)", forKey: "\(key)")
                }
            }
        }
    }
    
    
    
    struct Results {
        var data: Data?
        var response: Response?
        var error: Error?
        
        init(withData data: Data?, response: Response?, error: Error?) {
            self.data = data
            self.response = response
            self.error = error
        }
        
        init(withError error: Error) {
            self.error = error
        }
    }
    
    
    
    enum CustomError: Error {
        case failedToCreateRequest
    }
}


// MARK: - Custom Error Description
extension RestManager.CustomError: LocalizedError {
    public var localizedDescription: String {
        switch self {
        case .failedToCreateRequest: return NSLocalizedString("Unable to create the URLRequest object", comment: "")
        }
    }
}



class RestAPI{
    static let shared = RestAPI()
    
    let rest = RestManager()
    
    enum StatusCode {
        static let successGetRequest:Int = 200
        static let successPostRequest:Int = 201
        static let successDeleteRequest:Int = 200
        static let successPutRequest:Int = 200
    }
    
    
    func getData<T:Codable>(url:URL,headerDict:[String:String],decodingType:T.Type,completion :@escaping(_ model:T?,NSError?)->Void){
        
        for (key,val) in headerDict{
            
            rest.requestHttpHeaders.add(value: val, forKey: key)
        }
        rest.makeRequest(toURL: url, withHttpMethod: .get) { result in
            if let data = result.data{
                do {
                    
                    
                    let tData = try JSONDecoder().decode(T.self, from: data)
                    
                    DispatchQueue.main.async {
                        
                        completion(tData,nil)
                    }
                } catch let error {
                    
                    completion(nil,error as NSError? ?? nil)
                }
                
            }else{
                completion(nil,result.error as NSError? ?? nil)
            }
        }
    }
    
    
    
    func getData<T:Codable>(url:URL,decodingType:T.Type,completion :@escaping(_ model:T?,NSError?)->Void){
        rest.makeRequest(toURL: url, withHttpMethod: .get) { result in
            if let data = result.data{
                do {
                    
                    
                    let tData = try JSONDecoder().decode(T.self, from: data)
                    
                    DispatchQueue.main.async {
                        
                        completion(tData,nil)
                    }
                } catch let error {
                    
                    completion(nil,error as NSError? ?? nil)
                }
                
            }else{
                completion(nil,result.error as NSError? ?? nil)
            }
        }
    }
    
    
    
    
    
    func postData<T:Codable>(url:URL,postDict:[String:String],body:Data?,headerDict:[String:String],decodingType:T.Type,completion :@escaping(_ model:T?,NSError?)->Void){
        
        
        for (key,val) in headerDict{
            
            rest.requestHttpHeaders.add(value: val, forKey: key)
        }
        for (key,val) in postDict{
            
            rest.httpBodyParameters.add(value: val, forKey: key)
        }
        if let body{
            rest.httpBody = body
        }
        
        rest.makeRequest(toURL: url, withHttpMethod: .post) { result in
            
            guard let response = result.response else { return completion(nil,result.error as NSError? ?? nil) }
            
            if response.httpStatusCode == StatusCode.successPostRequest{
                if let data = result.data{
                    do {
                        
                        
                        let tData = try JSONDecoder().decode(T.self, from: data)
                        
                        DispatchQueue.main.async {
                            
                            completion(tData,nil)
                        }
                    } catch let error {
                        
                        completion(nil,error as NSError? ?? nil)
                    }
                    
                }else{
                    completion(nil,result.error as NSError? ?? nil)
                }
            }else{
                completion(nil,result.error as NSError? ?? nil)
            }
        }
    }
    
    
    func deleteData<T:Codable>(url:URL,decodingType:T.Type,completion :@escaping(_ model:T?,NSError?)->Void){
        rest.makeRequest(toURL: url, withHttpMethod: .delete) { result in
            
            
            guard let response = result.response else { return completion(nil,result.error as NSError? ?? nil) }
            
            if response.httpStatusCode == StatusCode.successDeleteRequest{
                if let data = result.data{
                    do {
                        
                        
                        let tData = try JSONDecoder().decode(T.self, from: data)
                        
                        DispatchQueue.main.async {
                            
                            completion(tData,nil)
                        }
                    } catch let error {
                        
                        completion(nil,error as NSError? ?? nil)
                    }
                }else{
                    completion(nil,result.error as NSError? ?? nil)
                }
            }else{
                completion(nil,result.error as NSError? ?? nil)
            }
        }
    }
    
    
    
    func updateData<T:Codable>(url:URL,updateDict:[String:String],headerDict:[String:String],decodingType:T.Type,completion :@escaping(_ model:T?,NSError?)->Void){
        
        
        for (key,val) in headerDict{
            
            rest.requestHttpHeaders.add(value: val, forKey: key)
        }
        
        guard let jsonData = try? JSONEncoder().encode(updateDict) else {
            print("Error: Trying to convert model to JSON data")
            return
        }
        
        rest.httpBody = jsonData
        
        rest.makeRequest(toURL: url, withHttpMethod: .put) { result in
            
            guard let response = result.response else {return completion(nil,result.error as NSError? ?? nil) }
            
            
            print(response.httpStatusCode)
            
            if response.httpStatusCode == StatusCode.successPutRequest{
                if let data = result.data{
                    do {
                        
                        
                        let tData = try JSONDecoder().decode(T.self, from: data)
                        
                        DispatchQueue.main.async {
                            
                            completion(tData,nil)
                        }
                    } catch let error {
                        
                        completion(nil,error as NSError? ?? nil)
                    }
                    
                }else{
                    completion(nil,result.error as NSError? ?? nil)
                }
            }else{
                completion(nil,result.error as NSError? ?? nil)
            }
        }
    }
    
    
    
    //MARK:- Patch incomplete
    
    func patchData<T:Codable>(url:URL,updateDict:[String:String],headerDict:[String:String],decodingType:T.Type,completion :@escaping(_ model:T?,NSError?)->Void){
        
        
        for (key,val) in headerDict{
            
            rest.requestHttpHeaders.add(value: val, forKey: key)
        }
        
        guard let jsonData = try? JSONEncoder().encode(updateDict) else {
            print("Error: Trying to convert model to JSON data")
            return
        }
        
        rest.httpBody = jsonData
        
        rest.makeRequest(toURL: url, withHttpMethod: .patch) { result in
            
            guard let response = result.response else {return completion(nil,result.error as NSError? ?? nil) }
            
            
            print(response.httpStatusCode)
            
            if response.httpStatusCode == StatusCode.successPutRequest{
                if let data = result.data{
                    do {
                        
                        
                        let tData = try JSONDecoder().decode(T.self, from: data)
                        
                        DispatchQueue.main.async {
                            
                            completion(tData,nil)
                        }
                    } catch let error {
                        
                        completion(nil,error as NSError? ?? nil)
                    }
                    
                }else{
                    completion(nil,result.error as NSError? ?? nil)
                }
            }else{
                completion(nil,result.error as NSError? ?? nil)
            }
        }
    }
}
