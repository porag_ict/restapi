//
//  ViewController.swift
//  RestManager
//
//  Created by Gabriel Theodoropoulos.
//  Copyright © 2019 Appcoda. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    
    let rest = RestManager()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        getUserByID(id: 1)
        postUser()
//        updateUser()
    }
    
    func getUserByID(id:Int){
        guard let url = URL(string: "https://reqres.in/api/users/\(id)") else { return }
        
        RestAPI.shared.getData(url: url, decodingType: Hello.self) { model, err in
            debugPrint(model?.data?.email)
            
        }
    }
    
    func postUser(){
        guard let url = URL(string: "https://reqres.in/api/users") else { return }

        var body = [String:String]()

        body["name"] = "John"
        body["job"] = "Developer"


        var header = [String:String]()
        header["Content-Type"] = "application/json"

        
        RestAPI.shared.postData(url: url, postDict: body, body: nil, headerDict: header, decodingType: Welcome.self) { model, err in
            
        }
    }
    
    
    func updateUser(){
        guard let url = URL(string: "https://reqres.in/api/users/2") else {return}
        
        var body = [String:String]()
        
        body["name"] = "John"
        
        
        var header = [String:String]()
        header["Content-Type"] = "application/json"
        
        
        RestAPI.shared.updateData(url: url, updateDict: body, headerDict: header, decodingType: Welcome.self) { result,err in
            
        }
    }
    
}
